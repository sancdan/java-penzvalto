
package probavizsga;

import hu.nyirszikszi.vizsga.VizsgaMegoldas1;
import hu.nyirszikszi.vizsga.VizsgaMegoldas2;


public class ProbaVizsgaA {


    public static void main(String[] args) {
        
        VizsgaMegoldas1 vm = new VizsgaMegoldas1(454,233);
        System.out.println(vm.getOsszeg());
        System.out.println(vm.getKulonbseg());
        
        System.out.println("----------");
        
        VizsgaMegoldas2 vm2 = new VizsgaMegoldas2(158500);
        
        vm2.felvalt(3000);
        System.out.println("Kiadott háromezresek: \t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        

        vm2.felvalt(5000);
        System.out.println("Kiadott ötezres: \t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        vm2.felvalt(2000);
        System.out.println("Kiadott kétezres: \t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        vm2.felvalt(1000);
        System.out.println("Kiadott ezres: \t\t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        vm2.felvalt(500);
        System.out.println("Kiadott ötszázas: \t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        
        System.out.println("----------");
    }
    
}
