
package hu.nyirszikszi.vizsga;


public class VizsgaMegoldas1 {
    private int a;
    private int b;

    public VizsgaMegoldas1(int a, int b) {
        this.a = a;
        this.b = b;
    }
    
    public int getOsszeg(){
        return a+b;
    }
    
    public  int getKulonbseg(){
        return a-b;
    }
}
