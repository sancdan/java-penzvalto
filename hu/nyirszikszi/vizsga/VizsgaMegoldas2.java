
package hu.nyirszikszi.vizsga;


public class VizsgaMegoldas2 {

	// osztályváltozók, ezek elérhetők az egész osztályon belül,
	// minden függvényben és metódusban
    int bentlevoOsszeg;
    int cimletDb;
    
    int kc1 = 5000;
    int kc2 = 2000;
    int kc3 = 1000;
    int kc4 = 500;
	// osztályváltozók vége
    
	
	// Ez a VizsgaMegoldas2 nevű osztály egy konstruktora
	// Ezt onnan tudjuk, hogy ugyanaz a neve, mint az osztálynak
	// Ez minden esetben lefut, amikor példányosítjuk az osztályt
	// Amikor példányosítjuk az osztályt akkor ugye meghívódik ez a konstruktor
	// annak átadunk egy összeget, amit itt "osszeg" nevű paraméternek nevezünk
	// A példányosítás így néz ki pl.: VizsgaMegoldas2 vm2 = new VizsgaMegoldas2(158500);
	// A példában az osszeg paraméter = 158500 értéket kap
	  //                   ez itt lent
	  //                      |
	  //                      V
    public VizsgaMegoldas2(int osszeg){
		// A this. azt jelenti, hogy a fenti osztályváltozóra hivatkozok vele 
		// (de a this nem kötelező, ha egyértelmű, hogy melyik változóról van szó)
		// A bentlevoOsszeg osztályváltozót egyenlővé teszem az osszeg paraméterrel (158500)
        this.bentlevoOsszeg = osszeg;
    }
    
	// ennek a függvénynek a meghívása a mainben így néz ki: vm2.felvalt(5000);
	// a zárójelben átadom, hogy milyen címletre akarom váltani, jelen esetben 5000-resekbe
    public void felvalt(int cimlet){
		// itt megvizsgálom, hogy ez a címlet engedélyezett-e, amit egy másik
		// funkció végez el. Lentebb látható a hasCimlet() függvény
		// ez true-val tér vissza ha engedélyezett és false-sal ha nem.
		if(this.hasCimlet(cimlet)){
			//ha true-val tér vissza a hasCimlet() függvény akkor ebbe az ágba kerülünk
			// a következő sorban a cimletDb osztályváltozónak fogok értéket adni
			// méghozzá úgy, hogy a bent lévő összeget (158500) elosztom a címlettel (5000)
			// az osztás végeredménye 31,76 db ötezrest adna vissza, 
			// de mivel a cimletDb változó int tipusú ezért csak a 31-et kapja meg a tizedesvessző
			// utáni rész elszáll. Tehát így megtudtuk, hogy 31 db ötezres címlet adható ki, amely
			// értéket itt beállítunk a this.cimletDb osztályváltozónak
            this.cimletDb = this.bentlevoOsszeg / cimlet;
			
			// viszont a maradékkal is kezdenünk kell valamit ezért maradékos osztást végzünk
			// a bent lévő összeggel és a kért címlettel, hogy megtudjuk a címletek kiadása után
			// mennyi pénz marad bent. Jelen esetben 158500/5000 megvan 31-szer ugye ezt már tudjuk
			// az 31*5000 egyenlő 155000-rel, marad 3500.
			// ennek a maradékos osztásnak vagy mod-nak (jele a % jel) az a lényege, hogy a maradékot adja vissza
			// tehát jelen esetben a this.bentlevoOsszeg = 3500 lesz, amivel felülírjuk
			// az alapból, a konstruktorban beállított 158500-at.
			// innentől kezdve a bentlevoOsszeg értéke már 3500 lesz, így a ha újra meghívjuk
			// a felvalt() metódust akkor már ezzel az értékkel fog számolni
            this.bentlevoOsszeg = this.bentlevoOsszeg % cimlet;
        }else{
			// ha a hasCimlet() false-sal tér vissza akkor ez az ág fut le
            System.out.println(cimlet + " címletet nem tudok kiadni!");
        }
    }
    
	// ez a funkció csak visszaadja a a this.cimletDb változó értékét, amit az előbb állítottunk be
	// a felvalt() metódusban
    public String getCimletDb(){
        return this.cimletDb + " db. ";
    }

	// ez a függvény vissaadja a this.bentlevoOsszeg értékét, amit az előbb állítottunk be
	// a felvalt() metódusban
    public String getBentlevoOsszeg(){
        return "Maradt " + this.bentlevoOsszeg + " Ft";
    }
    
	// ez a függvény ellenőrzi, hogy kiadható-e a kért címlet
	// a felvalt() metódusban kerül meghívásra
    public boolean hasCimlet(int valto){
        if(valto == kc1 || valto == kc2 || valto == kc3 || valto == kc4){
            return true;
        }
            return false;
    }
	
	// Tehát miután lefutottak a fenti függvények, metódusok az 5000-res címlettel, akkor megkapjuk, hogy
	// 31 db 5000-res adható ki, és a fennmaradó bent lévő összeg 3500 Ft, és ezt el is tároljuk az osztályváltozókban
	// Ezután mikor újra meghívjuk a függvényeket, metódusokat, akkor már 3500 Ft-ról indul az egész.
	// Így néz ki a példányosítás
	
	/*
		VizsgaMegoldas2 vm2 = new VizsgaMegoldas2(158500);
        
        // ez a két sor itt csak egy próba hívás arra, hogy adjon ki 3000-res címletet
		// itt azzal fog visszatérni, hogy nem adható ki ilyen címlet
		vm2.felvalt(3000);
        System.out.println("Kiadott háromezresek: \t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        
		// itt pedig a működő rész. Meghívjuk 5000-res, 2000-res, ezres és 500-as címletekkel
        vm2.felvalt(5000);
        System.out.println("Kiadott ötezres: \t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        vm2.felvalt(2000);
        System.out.println("Kiadott kétezres: \t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        vm2.felvalt(1000);
        System.out.println("Kiadott ezres: \t\t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
        vm2.felvalt(500);
        System.out.println("Kiadott ötszázas: \t" + vm2.getCimletDb() + vm2.getBentlevoOsszeg());
	
	*/
    
    
}
